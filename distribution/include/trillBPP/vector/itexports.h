/*
 * itexports.h
 *
 *  Created on: 29-Aug-2017
 *      Author: shivujagga
 */

#ifndef TRILLBPP_ITEXPORTS_H_
#define TRILLBPP_ITEXPORTS_H_


/*defined from cmake when using the shared version of IT++ library*/
//#cmakedefine TRILL_SHARED_LIB

/*needed to export shared library symbols on Windows*/
#if defined(TRILL_SHARED_LIB) && defined(_MSC_VER)
  #ifndef TRILL_EXPORT
    #if defined(trill_EXPORTS) || defined(trill_debug_EXPORTS) /*automatically defined by cmake*/
      #define TRILL_EXPORT __declspec(dllexport)
    #else
      #define TRILL_EXPORT __declspec(dllimport)
    #endif
  #endif
#endif

#if (__GNUC__ >= 4) /*UNIX*/
  #ifndef TRILL_EXPORT_TEMPLATE
    #define TRILL_EXPORT_TEMPLATE extern
  #endif
#endif

#ifndef TRILL_EXPORT
  #define TRILL_EXPORT
#endif
#ifndef TRILL_EXPORT_TEMPLATE
  #define TRILL_EXPORT_TEMPLATE
#endif

#endif
