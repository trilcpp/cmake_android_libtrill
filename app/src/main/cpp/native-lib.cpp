#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_network_distriintegre_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}


#include <iostream>
#include <fftw3.h>
#include <android/log.h>
#include <trillBPP/vector/vec.h>

using namespace std;

extern "C"
JNIEXPORT jint JNICALL
Java_com_network_distriintegre_MainActivity_main(){
    cout<<"welcome bOE:"<<endl;
    //double pl = check*o;
    //cout<<pl<<endl;
    trill::vec a;
    a="5";
    int data_bits[6]={1,2,3,4,5,7};
    int rrc_wave[3]={1,2,4};
        int lenA = sizeof(data_bits)/sizeof(int);
        int lenB = sizeof(rrc_wave)/sizeof(int);
        int lenC = lenA + lenB -1;
        int c[lenC];
//Complex data-type for inputting our data_bits and rrc_wave
        fftw_complex in_data[lenC];
        fftw_complex in_rrc[lenC];
//@flag - to identify which  is maxlength, 0 for len a : 1 for lenb
        int flag;
        if(lenA>lenB)
            flag = 0;
        else flag = 1;
//Zero-padding the data from their size till lenC
        int i;
        if(flag == 0){
            for(i = 0; i < lenA; i++ ){
                in_data[i][0] = data_bits[i];
                in_data[i][1] = 0;
                if( i<lenB ){
                    in_rrc[i][0] = rrc_wave[i];
                    in_rrc[i][1] = 0;
                } else //else start zero-padding @rrc_wave
                { in_rrc[i][0] = 0;
                    in_rrc[i][1] = 0;
                }
            }
            for(i=lenA ; i < lenC; i++ ){	//ZeroPad till @lenC
                in_data[i][0] = 0;
                in_data[i][1] = 0;
                in_rrc[i][0] = 0;
                in_rrc[i][1] = 0;
            }
        }
        else{
            for(i = 0; i < lenB; i++ ){
                in_rrc[i][0] = rrc_wave[i];
                in_rrc[i][1] = 0;
                if( i<lenA ){
                    in_data[i][0] = data_bits[i];
                    in_data[i][1] = 0;
                } else ////else start zero-padding @data_bits
                { in_data[i][0] = 0;
                    in_data[i][1] = 0;
                }
            }
            for(i=lenB ; i < lenC; i++ ){	//ZeroPad till @lenC
                in_data[i][0] = 0;
                in_data[i][1] = 0;
                in_rrc[i][0] = 0;
                in_rrc[i][1] = 0;
            }
        }
//FFTW functions and variable introduced to store frequency
        fftw_complex freq_data[lenC], freq_rrc[lenC];
        fftw_complex rev_data[lenC], time_data[lenC];
        fftw_plan p1 = fftw_plan_dft_1d(lenC, in_data, freq_data, FFTW_FORWARD ,FFTW_ESTIMATE);
        fftw_plan p2 = fftw_plan_dft_1d(lenC, in_rrc, freq_rrc, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_plan rev = fftw_plan_dft_1d(lenC, rev_data, time_data,FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(p1);
        fftw_execute(p2);
//    double SCALE = lenC;
        for( i = 0; i < lenC ; i++ ) {
            double realD = freq_data[i][0];
            double imagD = freq_data[i][1];
            double realS = freq_rrc[i][0];
            double imagS = freq_rrc[i][1];
            rev_data[i][0] = (realD * realS - imagD * imagS);//*SCALE;
            rev_data[i][1] = (realD * imagS + imagD * realS);//*SCALE;
        }
        fftw_execute(rev);
        cout<<"-----------------------------------------------------";
        for( i = 0; i < lenC ; i++ ){
//In Inverse FFT in  C++ fftw, each data-point is scaled by the length of the array, here by @lenC
            c[i] = time_data[i][0]/lenC;
            __android_log_print(ANDROID_LOG_DEBUG, "LOG_TAG", "Need to print : %d",c[i]);
        }
//The following block deletes array portion from end and beginning to get the middle parts
//	int maxLen;
//	if(lenA > lenB) {
//		maxLen = lenA;
//	} else {
//		maxLen = lenB;
//	}
//	int diff = lenC - maxLen;
//	i = 0;
//	while(i < diff) {
//		c.del(c.size()-1);
//		i++;
//		if(i < diff) {
//			c.del(0);
//			i++;
//		}
//	}
        fftw_destroy_plan(p1); fftw_destroy_plan(p2); fftw_destroy_plan(rev);
        return c[0];

}
